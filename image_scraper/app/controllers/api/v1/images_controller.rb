require "open-uri"

module Api
  module V1
    class ImagesController < ApplicationController
      respond_to :json
      before_filter :cors_preflight_check
      after_filter :cors_set_access_control_headers
      
      def index
        respond_with Image.all.order_by(created_at: "desc")
      end
      
      def show
        
      end
      
      def create
        #There are not validations neither transactions
        new_ids = Array.new
        image_urls = params[:images_url].split(';')
        image_urls.each do |image_url|
          if not Image.where(:url => image_url).exists?
            @image = Image.new(:url => image_url)
            @image.image_binary = Moped::BSON::Binary.new(:generic, get_remote_image(image_url))
            @image.save
            new_ids.push @image.id
          end
        end

        render :json => Image.find(new_ids), :status => 200
        
      end
      
      def destroy
        #There are not validations neither transactions
        @image = Image.find(params[:id])
        if @image.destroy
          render :json => '{"response":"ok"}', :status => 200 and return
        end
      end
      
      private
      
      def get_remote_image(url)
        extname = "temp.png"
        extname = File.extname(url)
        basename = File.basename(url, extname)
        file = Tempfile.new([basename, extname])
        file.binmode
        open(URI.parse(url)) do |data|
          file.write data.read
        end
        file.rewind
        tmp = File.open(file, 'rb').read
        return Base64.encode64(tmp)
      end
      
      def cors_set_access_control_headers
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
        headers['Access-Control-Max-Age'] = "1728000"
      end

      def cors_preflight_check
        if request.method == :options
          headers['Access-Control-Allow-Origin'] = '*'
          headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
          headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
          headers['Access-Control-Max-Age'] = '1728000'
          render :text => '', :content_type => 'text/plain'
        end
      end
    end
  end
end