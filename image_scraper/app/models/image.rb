class Image
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :url
  field :image_binary, type: Moped::BSON::Binary
end
