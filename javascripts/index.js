function Exercise1() {
	var server = "http://ec2-107-20-239-210.compute-1.amazonaws.com:8089"
	
	this.init = function() {
		$.ajaxSetup({
		    beforeSend: function(){
		        $(".loading").css("display","inline");
		    },
			complete: function(){
		        $(".loading").hide();
		    }
		});
		
		scrapeAction();
		saveResult();
		deleteImage();
		cancelSelection();
		logoColors();
		showImages();
		
	}
	
	logoColors = function() {
		var colors = ["#174CF2","#E72432","#F8AB0F","#029913"];
		$(".logo ul").find("li").each(function () {
			$(this).css("color",colors[Math.floor(Math.random()*3)]);
		});
	}
	
	scrapeAction = function() {
		$("#scrape").live("click", function() {
			$.ajax({
			    url: $("#url").val(),
			    type: 'GET',
			    success: function(res) {
			        var headline = $(res.responseText).find('img').appendTo("<div>");
					
					$.each(headline, function(i, item) {
						console.log(item.attributes.src.value);
						$("#images-container").append(
							$("<div>", { 'class': "box" }).append(
								$("<img/>", {
									"onclick": "$(this).toggleClass('img-selected');",
									"src": item.attributes.src.value,
									'alt': (item.attributes.alt == undefined ? '' : item.attributes.alt.value)
								})
						));
					});
					
					$(".buttons-container").show();
					$(".wrapper").show();
					$('#images-container').imagesLoaded( function(){
						$('#images-container').masonry({
							itemSelector: '.box'
						});
					});

			    }
			});
		});
	}
	
	deleteImage = function() {
		$(".delete-image").live("click", function() {
			if(confirm("Are you sure to delete this image?")) {
				var image_id = $(this).data("image_id");
				$.ajax({
				    url: server + "/api/v1/images/" + image_id + ".json",
					dataType: "json",
				    type: 'POST',
					data: {"_method":"delete"},
				    success: function(data) {
						if(data.response == "ok") {
							$("#" + image_id).remove();
						}

				    }
				});
			}
		});
	}
	
	saveResult = function() {
		$("#save").live("click", function() {
			var new_selected = new Array;
	        $("#images-container").find(".img-selected").each(function () {
				new_selected.push(this.src);
	        });
			$("#new_selected").val(new_selected.join(";"));
			
			$.ajax({
			    url: server + "/api/v1/images.json",
				dataType: "json",
				data: {
					images_url: new_selected.join(";")
				},
				type: 'POST',
			    success: function(data) {
					console.log(data);
					$('.isnew').remove();
					$.each(data, function(i, item) {
						//console.log(item.image_binary.data);
						$("#selected-preview ul").prepend(
								$("<li/>", {"id":item._id}).append(
									$("<div/>", {"class":"image-container", "html":"<div class='isnew'>New</div><div class='delete-image' data-image_id='" + item._id + "'>X</div>"}).append(
										$("<img/>", {
											"src": "data:image/png;base64," + item.image_binary.data
						}))));
					});

			    }
			});
			
			hideFind();
		});
	}
	
	cancelSelection = function() {
		$("#cancel").live("click", function() {
			hideFind();
		});
	}
	
	hideFind = function() {
		$(".buttons-container").hide();
		$('[class^="wrapper"]').hide();
		$("#images-container").off().empty().masonry("destroy");
	}
	
	showImages = function() {
		$.ajax({
		    url: server + "/api/v1/images.json",
			dataType: "json",
		    type: 'GET',
			data: {},
		    success: function(data) {
				console.log(data);
				$.each(data, function(i, item) {
					//console.log(item.image_binary.data);
					$("#selected-preview ul").append(
							$("<li/>", {"id":item._id}).append(
								$("<div/>", {"class":"image-container", "html":"<div class='delete-image' data-image_id='" + item._id + "'>X</div>"}).append(
									$("<img/>", {
										"src": "data:image/png;base64," + item.image_binary.data
					}))));
				});
				
		    }
		});
	}
};

$(function () {
	var exercise1 = new Exercise1();
	exercise1.init();
});